# Importamos las librerías
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
# Importamos los datos
bitcoin = pd.read_csv('BTC-USD.csv', date_parser = True)
bitcoin.tail()

# Seleccionamos solo los datos de CLose
data = bitcoin.reset_index()['Open']
data

# Vemos cuantos datos tenemos
data.shape

plt.figure(figsize=(14,5))
plt.plot(data)

# Escalamos los datos
from sklearn.preprocessing import MinMaxScaler
scaler=MinMaxScaler(feature_range=(0,1))
data=scaler.fit_transform(np.array(data).reshape(-1,1))

print(data)

# Dividimos los datos en train y test
training_size=int(len(data)*0.8)
test_size=len(data)-training_size
train_data,test_data=data[0:training_size],data[training_size:len(data)]

train_data.shape, test_data.shape

# Variable independiente y dependiente de train
X_train = [] 
Y_train = []

# Ventaneamos los datos
for i in range(60, train_data.shape[0]):
    X_train.append(train_data[i-60:i])
    Y_train.append(train_data[i,0])
    
# Variable independiente y dependiente de train
X_test = [] 
Y_test = []

# Ventaneamos los datos
for i in range(60, test_data.shape[0]):
    X_test.append(test_data[i-60:i])
    Y_test.append(test_data[i,0])

X_train, Y_train = np.array(X_train), np.array(Y_train)
X_test, Y_test = np.array(X_test), np.array(Y_test)

print(X_train.shape), print(Y_train.shape)

# Le hacemos un reshape a los datos [datos, tamaño ventana, num variables] 
# Se necesita para la LSTM
X_train =X_train.reshape(X_train.shape[0],X_train.shape[1] , 1)
X_test = X_test.reshape(X_test.shape[0],X_test.shape[1] , 1)

### Creamos el modelo
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dropout

model = Sequential()
model.add(LSTM(units = 50, activation = 'relu', return_sequences = True, input_shape = (X_train.shape[1], 1)))
model.add(Dropout(0.2)) 
model.add(LSTM(units = 50, activation = 'relu', return_sequences = True))
model.add(Dropout(0.2)) 
model.add(LSTM(units = 50, activation = 'relu', return_sequences = True))
model.add(Dropout(0.2)) 
model.add(LSTM(units = 50, activation = 'relu'))
model.add(Dropout(0.2)) 
model.add(Dense(units =1))
model.summary()

model.compile(optimizer = 'adam', loss = 'mean_squared_error')
history= model.fit(X_train, Y_train, epochs = 25, batch_size =50, validation_split=0.1)

# Hacemos las predicciones
train_predict=model.predict(X_train)
test_predict=model.predict(X_test)

# Transformamos a su forma original
train_predict=scaler.inverse_transform(train_predict)
test_predict=scaler.inverse_transform(test_predict)

# Calculamos el RMSE
import math
from sklearn.metrics import mean_squared_error
math.sqrt(mean_squared_error(Y_train,train_predict))

 # Movemos los datos de train para graficar
look_back=60
trainPredictPlot = np.empty_like(data)
trainPredictPlot[:, :] = np.nan
trainPredictPlot[look_back:len(train_predict)+look_back, :] = train_predict
# Movemos los datos de test para graficar
testPredictPlot = np.empty_like(data)
testPredictPlot[:, :] = np.nan
testPredictPlot[len(train_predict)+(look_back*2):len(data), :] = test_predict
# Graficamos
plt.plot(scaler.inverse_transform(data))
plt.plot(trainPredictPlot)
plt.plot(testPredictPlot)
plt.show()

temp_input=list(x_input)
temp_input=temp_input[0].tolist()

temp_input

# Calculamos la prediccion de los 10 siguientes dias
from numpy import array

lst_output=[]
n_steps=60
i=0
while(i<10):
    
    if(len(temp_input)>60):
        #print(temp_input)
        x_input=np.array(temp_input[1:])
        print("{} day input {}".format(i,x_input))
        x_input=x_input.reshape(1,-1)
        x_input = x_input.reshape((1, n_steps, 1))
        #print(x_input)
        yhat = model.predict(x_input, verbose=0)
        print("{} day output {}".format(i,yhat))
        temp_input.extend(yhat[0].tolist())
        temp_input=temp_input[1:]
        #print(temp_input)
        lst_output.extend(yhat.tolist())
        i=i+1
    else:
        x_input = x_input.reshape((1, n_steps,1))
        yhat = model.predict(x_input, verbose=0)
        print(yhat[0])
        temp_input.extend(yhat[0].tolist())
        print(len(temp_input))
        lst_output.extend(yhat.tolist())
        i=i+1
    

print(lst_output)

day_new=np.arange(1,61)
day_pred=np.arange(61,71)

import matplotlib.pyplot as plt

plt.plot(day_new,scaler.inverse_transform(data[1767:]))
plt.plot(day_pred,scaler.inverse_transform(lst_output))